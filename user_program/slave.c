#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>

#define PAGE_SIZE 4096
#define BUF_SIZE 512
#define COPY_SIZE 2048

int main (int argc, char* argv[])
{
	char buf[BUF_SIZE];
	int dev_fd, file_fd;// the fd for the device and the fd for the input file
	int decrement, count;
	size_t ret, file_size = 0;
	char file_name[50];
	char method[20];
	char ip[20];
	char *mmap_addr = NULL, *dest;
	struct timeval start;
	struct timeval end;
	double trans_time; //calulate the time between the device is opened and it is closed

	strcpy(file_name, argv[1]);
	strcpy(method, argv[2]);
	strcpy(ip, argv[3]);

	if( (dev_fd = open("/dev/slave_device", O_RDWR)) < 0){//should be O_RDWR for PROT_WRITE when mmap()
	
		perror("failed to open /dev/slave_device\n");
		return 1;
	}
	gettimeofday(&start ,NULL);
	if( (file_fd = open (file_name, O_RDWR | O_CREAT | O_TRUNC)) < 0) {
		perror("failed to open input file\n");
		return 1;
	}

	//Get mmap for spin lock.
	mmap_addr = (char *)mmap(0, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, dev_fd, 0);
	if(mmap_addr == NULL) {
		perror("mmap failed\n");
		return 1;
	}

	//Connect to master in the device
	mmap_addr[0]=1;
	if(ioctl(dev_fd, 0x12345677, ip) == -1) {
		perror("ioclt create slave socket error\n");
		return 1;
	}
	while(mmap_addr[0]);

	//first get the file_size
	read(dev_fd, buf, sizeof(buf));
	file_size = atoi(buf);
	ftruncate(file_fd, file_size);

	// Get the file from master.
	dest = mmap(NULL, file_size, PROT_READ | PROT_WRITE, MAP_SHARED, file_fd, 0);
	decrement = file_size;
	count = 0;
	switch(method[0]){
		case 'f'://fcntl : read()/write()
			do{
				ret = read(dev_fd, buf, sizeof(buf)); // read from the the device
				write(file_fd, buf, ret); //write to the input file
			} while(ret > 0);
			break;
		case 'm': //mmap :
			while(decrement >= COPY_SIZE){
				mmap_addr[0] = 1;
				if(ioctl(dev_fd, 0x12345678, COPY_SIZE) == -1){
					perror("ioctl mmap receive failed\n");
					return 1;
				}
				while(mmap_addr[0]);
				memcpy(dest + count * COPY_SIZE, mmap_addr + 1, COPY_SIZE);
				++count;
				decrement -= COPY_SIZE;
			}
			if(decrement > 0){
				mmap_addr[0] = 1;
				if(ioctl(dev_fd, 0x12345678, decrement) == -1){
					perror("ioctl mmap receive failed\n");
					return 1;
				}
				while(mmap_addr[0]);
				memcpy(dest + count * COPY_SIZE, mmap_addr + 1, decrement);
			}
			break;
	}
	
	// end receiving data, close the connection
	mmap_addr[0] = 1;
	if(ioctl(dev_fd, 0x12345679) == -1){
		perror("ioclt client exits error\n");
		return 1;
	}
	while(mmap_addr[0]);

	//Get transmission information
	gettimeofday(&end, NULL);
	trans_time = (end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)*0.0001;
	printf("Transmission time: %lf ms, File size: %d bytes\n", trans_time, (int)file_size);
	
	//get page descriptor
	ioctl(dev_fd, 0x12345670);

	munmap(mmap_addr, PAGE_SIZE);
	munmap(dest, file_size);
	close(file_fd);
	close(dev_fd);
	return 0;
}


