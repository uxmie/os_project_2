#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>

#define PAGE_SIZE 4096
#define COPY_SIZE 2048
#define BUF_SIZE 512
size_t get_filesize(const char* filename);//get the size of the input file


int main (int argc, char* argv[])
{
	char buf[BUF_SIZE];
	int dev_fd, file_fd;// the fd for the device and the fd for the input file
	size_t ret, file_size;
	char file_name[50], method[20];
	char *mmap_addr = NULL, *src;
	struct timeval start;
	struct timeval end;
	double trans_time; //calulate the time between the device is opened and it is closed
	char tempBuf[BUF_SIZE];
	int decrement;
	int count;

	strcpy(file_name, argv[1]);
	strcpy(method, argv[2]);

	if( (dev_fd = open("/dev/master_device", O_RDWR)) < 0){
		perror("failed to open /dev/master_device\n");
		return 1;
	}
	gettimeofday(&start ,NULL);
	if( (file_fd = open (file_name, O_RDWR)) < 0 ){
		perror("failed to open input file\n");
		return 1;
	}

	if( (file_size = get_filesize(file_name)) < 0){
		perror("failed to get filesize\n");
		return 1;
	}
	
	//We need the mmap for spin lock.
	mmap_addr = (char *)mmap(0, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, dev_fd, 0);
	if(mmap_addr == NULL){
		perror("mmap failed\n");
		return 1;
	}
	
	mmap_addr[0] = 1;
	if(ioctl(dev_fd, 0x12345677) == -1){ //0x12345677 : create socket and accept the connection from the slave
		perror("ioclt server create socket error\n");
		return 1;
	}
	while(mmap_addr[0]);
	
	//send file_size
	sprintf(tempBuf, "%d", (int)file_size);
	write(dev_fd, tempBuf, BUF_SIZE);
	
	//Send file
	src = mmap(NULL, file_size, PROT_READ, MAP_PRIVATE, file_fd, 0);
	decrement = file_size;
	count = 0;
	switch(method[0]){
		case 'f': //fcntl : read()/write()
			do{
				ret = read(file_fd, buf, sizeof(buf)); // read from the input file
				write(dev_fd, buf, ret);//write to the the device
			} while(ret > 0);
			break;
		case 'm': //mmap :
			while(decrement >= COPY_SIZE){
				memcpy(mmap_addr + 1, count * COPY_SIZE + src, COPY_SIZE);
				decrement -= COPY_SIZE;
				++count;
				
				mmap_addr[0] = 1;
				if(ioctl(dev_fd, 0x12345678, COPY_SIZE) == -1){
					perror("ioctl mmap send failed\n");
					return 1;
				}
				while(mmap_addr[0]);
			}
			if(decrement > 0){
				memcpy(mmap_addr + 1, count * COPY_SIZE + src, decrement);
				mmap_addr[0] = 1;
				if(ioctl(dev_fd, 0x12345678, decrement) == -1){
					perror("ioctl mmap send failed\n");
					return 1;
				}
				while(mmap_addr[0]);
			}
			break;
	}

	if(ioctl(dev_fd, 0x12345679) == -1){ // end sending data, close the connection
		perror("ioclt server exits error\n");
		return 1;
	}

	//Output transmission information
	gettimeofday(&end, NULL);
	trans_time = (end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)*0.0001;
	printf("Transmission time: %lf ms, File size: %d bytes\n", trans_time, (int)file_size);
	
	//get page descriptor
	ioctl(dev_fd, 0x12345670);

	munmap(src, file_size);
	munmap(mmap_addr, PAGE_SIZE);
	close(file_fd);
	close(dev_fd);

	return 0;
}

size_t get_filesize(const char* filename)
{
    struct stat st;
    stat(filename, &st);
    return st.st_size;
}
